### Clock

A basic desktop clock for X11. It uses time.h and simple Xt timer callbacks to update a label.


![screenshot](screenshots/dvclock_sample.png)

#### Feature (singular)

- Shows the time and date.


#### Requirements

- Motif 2.3.8
- X11/Xlib

#### Build Instructions

Compile:

```
make clean && make
```

Run:

```
./dvclock
```

Install:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```



#### License

This software is distributed free of charge under the BSD Zero Clause license.