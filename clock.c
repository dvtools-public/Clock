#include <stdlib.h> /* system() */
#include <stdio.h> /* snprintf(), fprintf() */
#include <string.h> /* strcpy() */
#include <time.h> /* time(), strftime() */

#include <Xm/Xm.h>
#include <Xm/MwmUtil.h>
#include <Xm/Form.h>
#include <Xm/Label.h>


void repeaterFunction(XtPointer timeLabel, XtIntervalId *id)
{
	char timeBuffer[256];
	
	/* gets the current time using time.h */
	time_t currentTime;
	time(&currentTime);
	
	strftime(timeBuffer, sizeof(timeBuffer), "%-I:%M:%S %p\n%a %b %-d %Y", localtime(&currentTime));
	
	XmString labelString = XmStringCreateLocalized(timeBuffer);
	XtVaSetValues(timeLabel,
		XmNlabelString, labelString,
	NULL);
	
	XmUpdateDisplay(timeLabel);
	
	XmStringFree(labelString);
	
	XtAppContext app = XtWidgetToApplicationContext(timeLabel);
	XtAppAddTimeOut(app, 100, repeaterFunction, (XtPointer)timeLabel);
}


int main(int argc, char *argv[])
{
/* define xt context and widgets */
	XtAppContext app;
	
	
/* open top level */
	Widget topLevel = XtVaAppInitialize(&app, "dvclock", NULL, 0, &argc, argv, NULL, NULL);
	XtVaSetValues(topLevel,
		XmNtitle, "Clock",
		
	NULL);
	
	int decor;
	XtVaGetValues(topLevel, XmNmwmDecorations, &decor, NULL);
		decor &= ~MWM_DECOR_BORDER;
	XtVaSetValues(topLevel, XmNmwmDecorations, decor, NULL);
	
	int func;
	XtVaGetValues(topLevel, XmNmwmFunctions, &func, NULL);
		func &= ~MWM_FUNC_CLOSE;
		func &= ~MWM_FUNC_MOVE;
	XtVaSetValues(topLevel, XmNmwmFunctions, func, NULL);
	
	
/* create form as manager widget */
	Widget mainForm = XtVaCreateManagedWidget("mainForm", xmFormWidgetClass, topLevel,
		XmNshadowThickness, 1,
	NULL);

/* create labels */
	Widget timeLabel = XtVaCreateManagedWidget("timeLabel", xmLabelWidgetClass, mainForm,
		XmNalignment, XmALIGNMENT_CENTER,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 5,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 5,
		
		XtVaTypedArg, XmNlabelString, XmRString, "00:00:00 AM\nDay Mon 00 Year", 4,
	NULL);
	
	/* close top level */
	XtRealizeWidget(topLevel);
	
	XtAppAddTimeOut(app, 100, repeaterFunction, (XtPointer)timeLabel);

/* enter processing loop */
	XtAppMainLoop(app);
	
	return 0;
}
